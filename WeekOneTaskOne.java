import java.util.*;

public class WeekOneTaskOne {

    public static void main(String[] args) {

        List<Integer> myArray = new ArrayList<>();
        int numberInput;

        try (Scanner input = new Scanner(System.in)) {
            while (input.hasNextInt()) {
                numberInput = input.nextInt();
                if (numberInput == 0) {
                    break;
                }
                myArray.add(numberInput);
            }
            System.out.print("The numbers you entered are: ");
            for (int number : myArray) {
                System.out.print(number);
            }
            System.out.println();
            System.out.println("The biggest number is: " + Collections.max(myArray));
            System.out.println("The smallest number is: " + Collections.min(myArray));

            System.out.println("The largest odd number is: " + maxOdd(myArray));
        }
    }

    public static int maxOdd(List<Integer> list) {
        Iterator<Integer> iterator = list.iterator();

        int max = 0;
        while (iterator.hasNext()) {
            int num = iterator.next();
            if (num % 2 == 1) {
                if (num > max) {
                    max = num;
                }
            }
        }
        return max;
    }
}

